package kame.nekotools.inventory.itemfix.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

public class ItemFixEnterSetNameEvent extends ItemFixBaseEvent {

    private static final HandlerList handlers = new HandlerList();

    public ItemFixEnterSetNameEvent(Player player, ItemStack item) {
        super(player, item);
    }

    @NotNull
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
