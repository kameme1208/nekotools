package kame.nekotools.inventory.itemfix.events;

import kame.tagapi.TagItem;
import kame.tagapi.TagNode;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.Stack;

public class ItemFixEnterEditTagEvent extends ItemFixBaseEvent {

    private static final HandlerList handlers = new HandlerList();
    private final TagItem tagItem;
    private final TagNode data;
    private final Stack<String> paths;
    private final int page;

    public ItemFixEnterEditTagEvent(Player player, TagItem item, TagNode data, Stack<String> paths, int page) {
        super(player, item.getItemStack());
        this.data = data;
        this.tagItem = item;
        this.paths = paths;
        this.page = page;
    }

    @NotNull
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public TagItem getTagItem() {
        return tagItem;
    }

    public TagNode getData() {
        return data;
    }

    public Stack<String> getPaths() {
        return paths;
    }

    public int getPage() {
        return page;
    }
}
