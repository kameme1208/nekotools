package kame.nekotools.inventory.invswitch.repositories;

import kame.nekotools.inventory.invswitch.repositories.models.PlayerData;
import kame.nekotools.inventory.invswitch.repositories.yaml.YamlPlayerDataStore;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public interface PlayerDataStore {

    boolean hasData(@NotNull String name);

    PlayerData getData(@NotNull String name);

    void setData(@NotNull Player player, @NotNull String name);

    String getSelectedName();

    void setSelectedName(@NotNull String name);

    void save();

    static PlayerDataStore createNew(@NotNull UUID uuid) {
        return new YamlPlayerDataStore(uuid);
    }

    static PlayerDataStore createTemplate() {
        return new YamlPlayerDataStore("template");
    }
}
