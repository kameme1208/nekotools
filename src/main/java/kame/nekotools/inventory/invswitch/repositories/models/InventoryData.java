package kame.nekotools.inventory.invswitch.repositories.models;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

public record InventoryData(@NotNull ItemStack[] items, ItemStack cursor) {
    public InventoryData() {
        this(new ItemStack[41], null);
    }

    public InventoryData(@NotNull Player player) {
        this(player.getInventory().getContents(), player.getItemOnCursor());
    }
}
