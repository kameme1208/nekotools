package kame.nekotools.inventory.invswitch;

import kame.nekotools.NekoTools;
import kame.nekotools.inventory.invswitch.repositories.KnownInventories;
import kame.nekotools.inventory.invswitch.repositories.PlayerDataStore;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class InventorySwitchManager {
    private static final InventorySwitchManager singleton = new InventorySwitchManager();

    public static InventorySwitchManager getInstance() {
        return singleton;
    }

    private final PlayerDataStore templateDataStore = PlayerDataStore.createTemplate();
    private final ConcurrentHashMap<UUID, PlayerDataStore> playerDataStore = new ConcurrentHashMap<>();
    private final KnownInventories knownInventories = new KnownInventories();

    private InventorySwitchManager() {
    }

    public void switchInventory(@NotNull Player player, @NotNull String inventoryName) {
        var table = playerDataStore.computeIfAbsent(player.getUniqueId(), PlayerDataStore::createNew);
        var selected = table.getSelectedName();
        if (knownInventories.isDefaultType(selected)) {
            table.setData(player, selected);
        }
        if (knownInventories.isDefaultType(inventoryName)) {
            table.getData(inventoryName).setPlayerData(player);
        } else if (knownInventories.isStaticType(inventoryName)) {
            templateDataStore.getData(inventoryName).setPlayerData(player);
        }
        table.setSelectedName(inventoryName);
        Bukkit.getScheduler().runTaskAsynchronously(NekoTools.getInstance(), table::save);
        player.saveData();
    }

    public String getSelectedInventory(@NotNull Player player) {
        return playerDataStore.computeIfAbsent(player.getUniqueId(), PlayerDataStore::createNew).getSelectedName();
    }

    public boolean unloadPlayer(UUID playerId) {
        return playerDataStore.remove(playerId) != null;
    }

    public Map<? extends String, ? extends InventoryStoreType> getKnownInventories() {
        return knownInventories.getKnownInventories();
    }

    public boolean setKnownInventoryType(String inventoryName, InventoryStoreType type) {
        return knownInventories.setKnownInventoryType(inventoryName, type);
    }

    public void setTemplateData(String inventoryName, Player player) {
        templateDataStore.setData(player, inventoryName);
        Bukkit.getScheduler().runTaskAsynchronously(NekoTools.getInstance(), templateDataStore::save);
    }

    public boolean hasTemplate(String invName) {
        return templateDataStore.hasData(invName);
    }
}
