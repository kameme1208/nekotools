package kame.nekotools.commands.arguments;

import com.mojang.brigadier.context.CommandContext;
import dev.jorel.commandapi.CommandAPIBukkit;
import dev.jorel.commandapi.arguments.CommandAPIArgumentType;
import dev.jorel.commandapi.arguments.SafeOverrideableArgument;
import dev.jorel.commandapi.executors.CommandArguments;
import kame.nekotools.commands.arguments.coordinates.ArgumentPosition;
import kame.nekotools.commands.arguments.coordinates.VectorPosition;

public class VectorPositionArgument extends SafeOverrideableArgument<VectorPosition, VectorPosition> {

    public VectorPositionArgument(String nodeName) {
        super(nodeName, CommandAPIBukkit.get()._ArgumentVec3(true), pos -> pos.getX() + " " + pos.getY() + " " + pos.getZ());
    }

    @Override
    public Class<VectorPosition> getPrimitiveType() {
        return VectorPosition.class;
    }

    @Override
    public CommandAPIArgumentType getArgumentType() {
        return CommandAPIArgumentType.LOCATION;
    }

    @Override
    public <Source> VectorPosition parseArgument(CommandContext<Source> cmdCtx, String key, CommandArguments previousArgs) {
        var node = cmdCtx.getNodes().stream()
                .filter(x -> x.getNode().getName().equals(key))
                .iterator().next().getRange();
        var data = node.get(cmdCtx.getInput());
        var args = data.split(" ");
        var isRelativeX = isRelative(args[0]);
        var isRelativeY = isRelative(args[1]);
        var isRelativeZ = isRelative(args[2]);
        var xValue = new ArgumentPosition(isRelativeX, parseDouble(args[0], isRelativeX));
        var yValue = new ArgumentPosition(isRelativeY, parseDouble(args[1], isRelativeY));
        var zValue = new ArgumentPosition(isRelativeZ, parseDouble(args[2], isRelativeZ));
        var isLocal = args[0].startsWith("^");
        return new VectorPosition(isLocal, xValue, yValue, zValue);
    }

    private double parseDouble(String str, boolean isRelative) {
        var parse = isRelative ? str.substring(1) : str;
        return parse.length() > 0 ? Double.parseDouble(parse) : 0;
    }

    private boolean isRelative(String str) {
        return str.startsWith("^") || str.startsWith("~");
    }
}