package kame.nekotools.commands.wrapper.executors;

import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import dev.jorel.commandapi.executors.CommandArguments;
import org.bukkit.command.CommandSender;

public interface CommandExecution<Sender extends CommandSender> {
    void execute(Sender sender, CommandArgs args) throws WrapperCommandSyntaxException;

    default void asApi(Sender sender, CommandArguments args) throws WrapperCommandSyntaxException {
        execute(sender, new CommandArgs(args));
    }
}
