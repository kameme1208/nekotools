package kame.nekotools.commands;

import dev.jorel.commandapi.RegisteredCommand;
import dev.jorel.commandapi.arguments.IntegerArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.command.CommandSender;

import java.util.*;

public class NekoToolsCommand {

    private static final String description =
            "§6Description:§r NekoToolsに関するコマンドを実行します。";

    public static final Set<RegisteredCommand> commands =
            new TreeSet<>(Comparator.comparing(RegisteredCommand::commandName));

    static {
        // nekotools
        Register.create("nekotools", description, "nekotools.command.nekotools")
                .withArguments(new LiteralArgument("help"))
                .withOptionalArguments(new IntegerArgument("page", 1))
                .executesPlayer(NekoToolsCommand::help)
                .register();
    }

    private static void help(CommandSender sender, CommandArgs args) {
        try {
            int page = args.getOrSupply("page", CommandArgs.value(1)) - 1;
            int size = commands.size() / 9 + 1;
            if (size < page) {
                sender.sendMessage("§c[NekoTools] そのページは存在しません");
                return;
            }
            sender.sendMessage("\n \n \n \n");

            List<RegisteredCommand> com = new ArrayList<>(commands)
                    .subList(page * 9, size == page + 1 ? commands.size() : page * 9 + 9);

            var text = new TextComponent("§2[NekoTools] §aコマンドリスト  ");
            {
                var comp = new TextComponent(page > 0 ? "§d§n前へ§r" : "§8前へ§r");
                if (page > 0) {
                    comp.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/nekotools help " + page));
                }
                text.addExtra(comp);
                text.addExtra(" ");
            }
            {
                var comp = new TextComponent(size > page + 1 ? "§d§n次へ§r" : "§8次へ§r");
                if (size > page + 1) {
                    comp.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/nekotools help " + (page + 2)));
                }
                text.addExtra(comp);
                text.addExtra(" ");
            }
            text.addExtra(String.format("§b§l" + "page§e:§6%d/%d", page + 1, size));
            sender.spigot().sendMessage(text);
            for (var line : com) {
                var comp = new TextComponent();
                var str = line.shortDescription().orElse("§6Description:").substring("§6Description:".length());
                var suggest = String.format("/%s%s", line.commandName(), line.argsAsStr().isEmpty() ? "" : " ");
                comp.addExtra("§6§l≫§a§n" + line.commandName() + "§r");
                comp.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, suggest));
                comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("§6§nクリックで挿入！")));
                sender.spigot().sendMessage(new TextComponent(comp, new TextComponent("§e:" + str)));
            }
            while (com.size() < 9) {
                sender.spigot().sendMessage(new TextComponent());
                com.add(null);
            }
        } catch (Throwable e) {
            sender.sendMessage("Spigotサーバーでの実行のみ有効です。");
            e.printStackTrace();
        }
    }

}
