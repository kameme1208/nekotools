package kame.nekotools.commands.entity;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.arguments.*;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import dev.jorel.commandapi.wrappers.FloatRange;
import kame.nekotools.commands.arguments.VectorPositionArgument;
import kame.nekotools.commands.arguments.coordinates.VectorPosition;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;

public class VecCommand {

    private static final String description =
            "§6Description:§r 対象のエンティティを指定した方向に飛ばします。";

    static {

        Register.create("vec", description, "nekotools.command.vec")
                .withArguments(new EntitySelectorArgument.ManyEntities("targets"))
                .withArguments(new VectorPositionArgument("motion"))
                .executes(VecCommand::vec)
                .register();

        Register.create("vec", description, "nekotools.command.vec")
                .withArguments(new EntitySelectorArgument.ManyEntities("targets"))
                .withArguments(new LiteralArgument("to"))
                .withArguments(new LocationArgument("location"))
                .withArguments(new DoubleArgument("speed"))
                .withArguments(new FloatRangeArgument("speed_range"))
                .executes(VecCommand::vecTo)
                .register();
    }

    private static void vecN(CommandSender sender, CommandArgs args) {
        args.getOrSupply("", CommandArgs.value(0));
    }

    private static int vec(CommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        var entities = args.getOrSupply("targets", CommandArgs::throwEntities);
        var motion = args.getOrSupply("motion", CommandArgs.thrown(VectorPosition.class));
        var successCount = 0;
        var vec = motion.getVector(sender);
        for (var entity : entities) {
            var move = entity.getVelocity();
            move.setX(motion.getX().isRelative() ? vec.getX() + move.getX() : vec.getX());
            move.setY(motion.getY().isRelative() ? vec.getY() + move.getY() : vec.getY());
            move.setZ(motion.getZ().isRelative() ? vec.getZ() + move.getZ() : vec.getZ());
            entity.setVelocity(move);
            successCount++;
        }
        if (successCount > 0) {
            sender.sendMessage(successCount + "体のエンティティに速度[" + motion + "]を与えました。");
        } else {
            throw CommandAPI.failWithString("該当するエンティティが見つかりませんでした。");
        }
        return successCount;
    }

    private static int vecTo(CommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        var entities = args.getOrSupply("targets", CommandArgs::throwEntities);
        var target = args.getOrSupply("location", CommandArgs.thrown(Location.class));
        var speed = args.getOrSupply("speed", CommandArgs.thrown(Double.class));
        var range = args.getOrSupply("speed_range", CommandArgs.thrown(FloatRange.class));
        var successCount = 0;
        for (var entity : entities) {
            var motion = target
                    .subtract(entity.getLocation())
                    .toVector()
                    .multiply(speed);
            var upper = range.getUpperBound();
            var lower = range.getLowerBound();
            var length = Math.min(Math.max(lower, motion.length()), upper);
            if (motion.length() > 0) {
                motion.normalize();
            }
            entity.setVelocity(motion.multiply(length));
            successCount++;
        }
        if (successCount > 0) {
            sender.sendMessage(successCount + "体のエンティティに指定した位置へ向かう速度を与えました。");
        } else {
            throw CommandAPI.failWithString("該当するエンティティが見つかりませんでした。");
        }
        return successCount;
    }
}
