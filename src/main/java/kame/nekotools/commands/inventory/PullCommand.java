package kame.nekotools.commands.inventory;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.arguments.EntitySelectorArgument;
import dev.jorel.commandapi.arguments.IntegerArgument;
import dev.jorel.commandapi.arguments.ItemStackArgument;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.TranslatableComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

public class PullCommand {

    private static final String description =
            "§6Description:§r (give互換) 対象のプレイヤーにアイテムを配布します。";

    static {
        Register.create("pull", description, "nekotools.command.temploc")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new ItemStackArgument("item"))
                .withOptionalArguments(new IntegerArgument("count"))
                .executes(PullCommand::pullItems)
                .register();
    }

    public static void pullItems(CommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        var item = args.getOrThrow("item", ItemStack.class);
        item.setAmount(args.getOrSupply("count", CommandArgs.value(1)));
        for (var target : args.getOrSupply("targets", CommandArgs::throwPlayers)) {
            for (var overed : target.getInventory().addItem(item.clone()).values()) {
                var stacks = new ArrayList<>(Collections.nCopies(overed.getAmount() / 64, overed.clone()));
                stacks.add(overed.clone());
                stacks.get(0).setAmount(64);
                stacks.get(stacks.size() - 1).setAmount(overed.getAmount() % 64);
                for (var drop : stacks) {
                    target.getWorld().dropItem(target.getLocation(), drop);
                }
            }
            var component = new TextComponent("[NekoTools] §r");
            component.addExtra(target.getName());
            component.addExtra("に[");
            var displayComponent = Optional.ofNullable(item.getItemMeta()).map(ItemMeta::getDisplayName)
                    .filter(x -> !x.isEmpty()).map(TextComponent::new).map(BaseComponent.class::cast)
                    .orElse(new TranslatableComponent(item.getTranslationKey()));
            displayComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(displayComponent.toPlainText())));
            component.addExtra(displayComponent);
            component.addExtra("]を" + item.getAmount() + "個与えました。");
            sender.spigot().sendMessage(component);
        }
        if (args.getOrSupply("targets", CommandArgs::throwPlayers).isEmpty()) {
            throw CommandAPI.failWithString("[NekoTools] プレイヤーが見つかりませんでした。");
        }
    }

}
