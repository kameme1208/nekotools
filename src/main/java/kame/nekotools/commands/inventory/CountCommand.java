package kame.nekotools.commands.inventory;

import dev.jorel.commandapi.arguments.EntitySelectorArgument;
import dev.jorel.commandapi.arguments.ItemStackPredicateArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import dev.jorel.commandapi.arguments.ObjectiveArgument;
import kame.nekocore.utils.ItemUtils;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.command.CommandSender;
import org.bukkit.scoreboard.Objective;

public class CountCommand {
    private static final String description =
            "§6Description:§r プレイヤーのインベントリのアイテムをカウントします。";

    static {
        // count @p #minecraft:item{nbt} scoreTag
        Register.create("count", description, "nekotools.command.count")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new LiteralArgument("#any"))
                .withOptionalArguments(new ObjectiveArgument("objective"))
                .executes(CountCommand::count)
                .register();
        Register.create("count", description, "nekotools.command.count")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withOptionalArguments(new ItemStackPredicateArgument("type"))
                .withOptionalArguments(new ObjectiveArgument("objective"))
                .executes(CountCommand::count)
                .register();
    }

    private static int count(CommandSender sender, CommandArgs args) {
        var targets = args.getOrSupply("targets", CommandArgs::throwPlayers);
        var typeFilter = args.getOrSupply("type", CommandArgs.itemPredicate(x -> true));
        var objective = args.getOptional("objective").map(Objective.class::cast);

        var counts = 0;
        for (var target : targets) {
            var count = ItemUtils.countItems(target.getInventory().getContents(), typeFilter);
            objective.map(x -> x.getScore(target.getName())).ifPresent(x -> x.setScore(count));
            sender.sendMessage(String.format("[NekoTools] %sの持ち物に%s個のアイテムが見つかりました", target.getName(), count));
            counts += count;
        }
        return counts;
    }
}
