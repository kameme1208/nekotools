package kame.nekotools.utils;

import org.bukkit.World;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

public class WorldUtils {
    private static Method getHandle;
    private static Field convertable;
    private static Field dimensionType;
    private static final Pattern pattern = Pattern.compile("ResourceKey\\[.*? / (.*)]");

    public static String getDimensionName(World world) {
        try {
            if (getHandle == null) getHandle = world.getClass().getMethod("getHandle");
            var worldServer = getHandle.invoke(world);
            if (convertable == null) convertable = worldServer.getClass().getDeclaredField("convertable");
            var convert = convertable.get(worldServer);
            if (dimensionType == null) dimensionType = convert.getClass().getField("dimensionType");
            var matcher = pattern.matcher(dimensionType.get(convert).toString());
            return matcher.find() ? matcher.group(1) : world.getName();
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }
}
