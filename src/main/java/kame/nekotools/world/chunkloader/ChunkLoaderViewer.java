package kame.nekotools.world.chunkloader;

import kame.nekocore.math.Line3d;
import kame.nekotools.NekoTools;
import org.bukkit.*;
import org.bukkit.entity.Player;

import java.util.stream.IntStream;

public class ChunkLoaderViewer {

    public static void showEffect(ChunkLoader chunkloader) {
        var loc = chunkloader.getLocation();
        var world = chunkloader.getWorld();
        var center = loc.getChunk().getBlock(8, loc.getBlockY(), 8).getLocation().add(0, -0.9, 0);
        var line0 = new Line3d(loc, center);
        var seq0 = IntStream.range(0, 10).mapToDouble(x -> x / 10.0).mapToObj(line0::lerp).iterator();
        Bukkit.getScheduler().runTaskTimer(NekoTools.getPlugin(NekoTools.class), task -> {
            if (!seq0.hasNext()) task.cancel();
            else world.spawnParticle(Particle.DRAGON_BREATH, seq0.next().toVector().toLocation(world), 0);
        }, 0, 1);
        var seq1 = IntStream.range(-1, 4).mapToDouble(x -> (x + 0.75) / 4).iterator();
        Bukkit.getScheduler().runTaskTimer(NekoTools.getPlugin(NekoTools.class), task -> {
            if (!seq1.hasNext()) task.cancel();
            else showEffect(loc.getChunk(), loc.getBlockY() - 1, seq1.next(), 2);
        }, 10, 2);
    }

    public static void showEffect(Player player, Chunk chunk, int offset) {
        var itr = IntStream.range(0, 2).mapToDouble(x -> (x + 0.75) / 2).iterator();
        while (itr.hasNext()) {
            showEffect(player, chunk, player.getLocation().getBlockY() / 5 * 5 + offset, itr.next(), 2);
        }
    }

    public static void showEffect(Player player, Chunk chunk, int height, double ratio, double spread) {
        var c = chunk.getBlock(8, height, 8).getLocation();
        var pos1 = new Line3d(c, c.clone().add(+8, 0.1, +8)).lerp(ratio).toVector().toLocation(chunk.getWorld());
        var pos2 = new Line3d(c, c.clone().add(+8, 0.1, -8)).lerp(ratio).toVector().toLocation(chunk.getWorld());
        var pos3 = new Line3d(c, c.clone().add(-8, 0.1, -8)).lerp(ratio).toVector().toLocation(chunk.getWorld());
        var pos4 = new Line3d(c, c.clone().add(-8, 0.1, +8)).lerp(ratio).toVector().toLocation(chunk.getWorld());
        player.spawnParticle(Particle.SOUL_FIRE_FLAME, pos1, 0);
        player.spawnParticle(Particle.SOUL_FIRE_FLAME, pos2, 0);
        player.spawnParticle(Particle.SOUL_FIRE_FLAME, pos3, 0);
        player.spawnParticle(Particle.SOUL_FIRE_FLAME, pos4, 0);
        drawLine(player, pos1, pos2, spread, Particle.FLAME);
        drawLine(player, pos2, pos3, spread, Particle.FLAME);
        drawLine(player, pos3, pos4, spread, Particle.FLAME);
        drawLine(player, pos4, pos1, spread, Particle.FLAME);
    }

    public static void showEffect(Chunk chunk, int height, double ratio, double spread) {
        var c = chunk.getBlock(8, height, 8).getLocation().add(0, 0.1, 0);
        var pos1 = new Line3d(c, c.clone().add(+8, 0, +8)).lerp(ratio).toVector().toLocation(chunk.getWorld());
        var pos2 = new Line3d(c, c.clone().add(+8, 0, -8)).lerp(ratio).toVector().toLocation(chunk.getWorld());
        var pos3 = new Line3d(c, c.clone().add(-8, 0, -8)).lerp(ratio).toVector().toLocation(chunk.getWorld());
        var pos4 = new Line3d(c, c.clone().add(-8, 0, +8)).lerp(ratio).toVector().toLocation(chunk.getWorld());
        chunk.getWorld().spawnParticle(Particle.SOUL_FIRE_FLAME, pos1, 0);
        chunk.getWorld().spawnParticle(Particle.SOUL_FIRE_FLAME, pos2, 0);
        chunk.getWorld().spawnParticle(Particle.SOUL_FIRE_FLAME, pos3, 0);
        chunk.getWorld().spawnParticle(Particle.SOUL_FIRE_FLAME, pos4, 0);
        drawLine(chunk.getWorld(), pos1, pos2, spread, Particle.FLAME);
        drawLine(chunk.getWorld(), pos2, pos3, spread, Particle.FLAME);
        drawLine(chunk.getWorld(), pos3, pos4, spread, Particle.FLAME);
        drawLine(chunk.getWorld(), pos4, pos1, spread, Particle.FLAME);
    }

    public static void drawLine(Player target, Location from, Location to, double spread, Particle particle) {
        IntStream.range(1, (int)(from.distance(to) * spread)).mapToDouble(x -> x / from.distance(to) / spread)
                .mapToObj(new Line3d(from, to)::lerp).map(x -> x.toVector().toLocation(target.getWorld()))
                .forEach(x -> target.spawnParticle(particle, x, 0));
    }

    public static void drawLine(World world, Location from, Location to, double spread, Particle particle) {
        IntStream.range(1, (int)(from.distance(to) * spread)).mapToDouble(x -> x / from.distance(to) / spread)
                .mapToObj(new Line3d(from, to)::lerp).map(x -> x.toVector().toLocation(world))
                .forEach(x -> world.spawnParticle(particle, x, 0));
    }
}
