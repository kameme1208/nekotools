package kame.nekotools.update;

import kame.repositories.DependencyFinder;
import kame.repositories.PackageVersion;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

public class PluginUpdater {

    public static Optional<PackageVersion> check(Plugin plugin, int group_id, String name, File file) throws IOException {
        var packages = new URL(DependencyFinder.getGroupPackageUrl(group_id));
        return DependencyFinder.getLatestVersion(packages, name).filter(x -> !x.version().equals(plugin.getDescription().getVersion()));
    }

    public static void download(PackageVersion pack, File file) throws IOException {
        var url = new URL(getDownloadUrl(pack));
        try (var stream = url.openStream()) {
            var path = Path.of("plugins", Bukkit.getUpdateFolder(), file.getName());
            Files.createDirectories(path.getParent());
            Files.copy(stream, path, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public static String getDownloadUrl(PackageVersion pack) {
        return "https://gitlab.com/api/v4/projects/%s/packages/maven/%s/%s/%s/%s-%s.jar"
                .formatted(pack.project_id(), pack.groupId(), pack.artifactId(), pack.version(), pack.artifactId(), pack.version());
    }
}
