package kame.nekocore.utils;

import dev.jorel.commandapi.wrappers.IntegerRange;

public class NumberUtils {

    public static int parse(String[] args, int position, int def) {
        return args.length > position ? parse(args[position], def) : def;
    }

    public static float parse(String[] args, int position, float def) {
        return args.length > position ? parse(args[position], def) : def;
    }

    public static double parse(String[] args, int position, double def) {
        return args.length > position ? parse(args[position], def) : def;
    }

    public static int parse(String str, int vec) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return vec;
        }
    }

    public static float parse(String str, float vec) {
        try {
            return Float.parseFloat(str);
        } catch (NumberFormatException e) {
            return vec;
        }
    }

    public static double parse(String str, double vec) {
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return vec;
        }
    }

    public static int parsePos(String str, int vec) {
        return str.charAt(0) == '~' ? vec + parse(str.substring(1), 0) : parse(str, 0);
    }

    public static double parsePos(String str, double vec) {
        return str.charAt(0) == '~' ? vec + parse(str.substring(1), 0)
                : str.contains(".") ? parse(str, 0f) : parse(str, 0) + 0.5d;
    }

    public static float parsePos(String str, float vec) {
        return str.charAt(0) == '~' ? vec + parse(str.substring(1), 0)
                : str.contains(".") ? parse(str, 0f) : parse(str, 0) + 0.5f;
    }

    public static IntegerRange parseIntRange(String str) {
        var index = str.indexOf("..");
        if (index > -1) {
            var start = str.substring(0, index);
            var end = str.substring(index + 2);
            var startValue = start.isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(start);
            var endValue = end.isEmpty() ? Integer.MAX_VALUE : Integer.parseInt(end);
            return new IntegerRange(startValue, endValue);
        } else {
            var i = Integer.parseInt(str);
            return new IntegerRange(i, i);
        }
    }
}
